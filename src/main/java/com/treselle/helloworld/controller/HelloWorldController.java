package com.treselle.helloworld.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloWorldController {

	@RequestMapping(value = "/hello", method = { RequestMethod.GET })
	@ResponseBody
	public Map<String, Object> sayHello() {
		Map<String, Object> resp = new HashMap<>();
		
		resp.put("resp", "Hello this is a spring boot application deployed as docker using GitLab pipeline.");
		
		return resp;
	}
}
